# Album

Aplicacion para mostrar albunes de fotos.

**Clonar repositorio**

```
$ git clone https://gitlab.com/sergio.pernas1/album-jueves.git 

$ cd album-jueves
```

**Crear imagen**

```
$ sudo docker build -t album-image .
```
**Desplegar contenedor**

```
$ sudo docker run -d -p 80:80 album-image
```

**Script de despliege**
```
$ curl https://gitlab.com/sergio.pernas1/album-jueves/-/raw/main/deploy.sh | sudo bash -
```
